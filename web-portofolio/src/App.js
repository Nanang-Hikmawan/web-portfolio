import logo from "./logo.svg";
import "./App.css";
import Navbar from "./Component/Navbar";
import Header from "./Component/Header";
import Body from "./Component/Body";
import Main from "./Component/Main";
import Wrapper from "./Component/Wrapper";
import Footer from "./Component/Footer";

const App = () => {
  return (
    <div className="App">
      <Navbar />
      <Header />
      <Body />
      <Main />
      <Wrapper />
      <Footer />
    </div>
  );
};

export default App;
