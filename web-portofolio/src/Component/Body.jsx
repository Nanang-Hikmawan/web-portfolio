import React from "react";
import "../index.css";
import "aos/dist/aos.css";

const Body = () => {
  return (
    <>
      <div id="about">
        <div className="mybody">
          <div class="container">
            <div class="row text-center mb-3">
              <div class="col">
                <h2>About Me</h2>
              </div>
            </div>
            <div class="row justify-content-center fs-5 text-center">
              <div
                class="col-md-4"
                data-aos="fade-right"
                data-aos-duration="2000"
                data-aos-offset="300"
                data-aos-easing="ease-in-sine"
              >
                <p>
                  Hi! Nanang here! I'm a Mobile Developer - React Native. I
                  started writing my code from October 2021. Back before I
                  became an Agriculture student and started my first job as
                  waiters and two months later I was appointed as Assistant
                  Manager in a fast food retail. Then, I continued my career as
                  a quality control and operator in a factory. After that I
                  decided to stop my career there, because I wanted to learn
                  something new, something different, and here I am now.
                </p>
              </div>
              <div
                class="col-md-4"
                data-aos="fade-left"
                data-aos-duration="2000"
                data-aos-offset="300"
                data-aos-easing="ease-in-out"
              >
                <p>
                  I'm happy where I am now, I like code, I enjoy writing with
                  code and until now I'm still developing my skills as a Mobile
                  Developer. I have skills in HTML5, CSS3, JavaScript, Redux,
                  Redux Saga, React-Native, React, React.JS, and Git. Not only
                  writing code but I'm also curious and interested about UI/UX
                  design, Front End Developer.
                </p>
              </div>
            </div>
            <div class="row text-center mb-3">
              <div class="col">
                <h3>My Skills</h3>
              </div>
            </div>
            <div className="carousel">
              <div
                id="carouselExampleFade"
                class="carousel slide"
                data-bs-ride="carousel"
              >
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <img
                      src={require("./Images/React1.jpg")}
                      class="d-block w-100"
                      alt="..."
                    />
                  </div>
                  <div class="carousel-item">
                    <img
                      src={require("./Images/Reactjs.jpg")}
                      class="d-block w-100"
                      alt="..."
                    />
                  </div>
                  <div class="carousel-item">
                    <img
                      src={require("./Images/reactnative.webp")}
                      class="d-block w-100"
                      alt="..."
                    />
                  </div>
                  <div class="carousel-item">
                    <img
                      src={require("./Images/htmlcssjs.jpg")}
                      class="d-block w-100"
                      alt="..."
                    />
                  </div>
                  <div class="carousel-item">
                    <img
                      src={require("./Images/git.webp")}
                      class="d-block w-100"
                      alt="..."
                    />
                  </div>
                </div>
                <button
                  class="carousel-control-prev"
                  type="button"
                  data-bs-target="#carouselExampleFade"
                  data-bs-slide="prev"
                >
                  <span
                    class="carousel-control-prev-icon"
                    aria-hidden="true"
                  ></span>
                  <span class="visually-hidden">Previous</span>
                </button>
                <button
                  class="carousel-control-next"
                  type="button"
                  data-bs-target="#carouselExampleFade"
                  data-bs-slide="next"
                >
                  <span
                    class="carousel-control-next-icon"
                    aria-hidden="true"
                  ></span>
                  <span class="visually-hidden">Next</span>
                </button>
              </div>
            </div>
          </div>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
            <path
              fill="#e2edff"
              fill-opacity="1"
              d="M0,288L60,288C120,288,240,288,360,272C480,256,600,224,720,218.7C840,213,960,235,1080,229.3C1200,224,1320,192,1380,176L1440,160L1440,320L1380,320C1320,320,1200,320,1080,320C960,320,840,320,720,320C600,320,480,320,360,320C240,320,120,320,60,320L0,320Z"
            ></path>
          </svg>
        </div>
      </div>
    </>
  );
};

export default Body;
