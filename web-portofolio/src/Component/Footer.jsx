import React from "react";
import "../index.css";

const Footer = () => {
  return (
    <>
      <footer className="footer">
        <div class="container">
          <div className="head">
            <p>Follow my social media</p>
          </div>
          <div class="row text-center justify-content-center">
            <a
              href="https://www.linkedin.com/in/nanang-hikmawan-24616a162/"
              class="text-black fw-normal text-decoration-none me-2"
            >
              <i class="bi bi-linkedin me-2"></i>
              LinkedIn
            </a>
            <a
              href="https://www.facebook.com/"
              class="text-black fw-normal text-decoration-none me-2"
            >
              <i class="bi bi-facebook ms-2 me-2"></i>
              Facebook
            </a>
            <a
              href="https://www.instagram.com/"
              class="text-black fw-normal text-decoration-none me-1"
            >
              <i class="bi bi-instagram me-2 ms-2"></i>
              Instagram
            </a>
            <a
              href="https://bit.ly/3LWHvrE"
              class="text-black fw-normal text-decoration-none"
            >
              <i class="bi bi-whatsapp me-1 ms-2"></i> WhatsApp
            </a>
          </div>
        </div>
        <div className="copy">
          <p>Copyrigth; 2022 by Nanang Hikmawan</p>
        </div>
      </footer>
    </>
  );
};

export default Footer;
