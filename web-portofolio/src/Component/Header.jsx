import React from "react";
import "../index.css";

const Header = () => {
  return (
    <>
      <div id="home">
        <div className="jumbotron text-center">
          <img
            src={require("../Component/Images/js.jpg")}
            alt="Nanang Hikmawan"
            width="200"
            className="img-circle img-thumbnail"
          />
          <h1 class="display-4">Nanang Hikmawan</h1>
          <p class="lead">Bachelor of Engineering & Mobile Engineer</p>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
            <path
              fill="#ffffff"
              fill-opacity="1"
              d="M0,160L60,176C120,192,240,224,360,229.3C480,235,600,213,720,186.7C840,160,960,128,1080,128C1200,128,1320,160,1380,176L1440,192L1440,320L1380,320C1320,320,1200,320,1080,320C960,320,840,320,720,320C600,320,480,320,360,320C240,320,120,320,60,320L0,320Z"
            ></path>
          </svg>
        </div>
      </div>
    </>
  );
};

export default Header;
