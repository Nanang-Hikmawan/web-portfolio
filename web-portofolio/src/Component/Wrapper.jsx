import React from "react";
import "../index.css";

const Wrapper = () => {
  return (
    <>
      <div id="contact">
        <div class="container">
          <div class="row text-center mb-3">
            <div class="col">
              <h2>Contact me on email</h2>
            </div>
          </div>
          <div class="row text-start justify-content-center">
            <div class="col-md-6">
              <form
                action="https://formsubmit.co/nanang.hikmawan.a@gmail.com"
                method="POST"
              >
                <div class="mb-3">
                  <label for="name" class="form-label">
                    Your Name
                  </label>
                  <input
                    type="text"
                    class="form-control"
                    id="Your name"
                    aria-describedby="name"
                    placeholder="Full name"
                  />
                </div>
                <div class="mb-3">
                  <label for="nanang.hikmawan.a@gmail.com" class="form-label">
                    Email
                  </label>
                  <input
                    type="email"
                    name="email"
                    class="form-control"
                    id="nanang.hikmawan.a@gmail.com"
                    aria-describedby="email"
                    placeholder="your@email.com"
                  />
                </div>
                <div class="mb-3">
                  <label for="message" class="form-label">
                    Message
                  </label>
                  <textarea
                    class="form-control"
                    id="message"
                    rows="3"
                    placeholder="Drop your message here!"
                  ></textarea>
                </div>
                <button type="submit" class="btn btn-primary mb-4">
                  Send
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Wrapper;
