import React from "react";
import "../index.css";
import "aos/dist/aos.css";

const Main = () => {
  return (
    <>
      <div id="projects">
        <div className="main">
          <div class="container">
            <div class="row text-center mb-3">
              <div class="col">
                <h4>My Portfolio</h4>
              </div>
            </div>
            <div class="row text-center justify-content-center">
              <div class="col-md-4 mb-3">
                <div
                  class="card"
                  data-aos="flip-right"
                  data-aos-duration="2000"
                  data-aos-delay="100"
                >
                  <img
                    src={require("../Component/Images/Cravyng.png")}
                    class="card-img-top"
                    alt="Projects1"
                  />
                  <div class="card-body">
                    <p class="card-text">
                      Cravyng is an online application for selling and buyying
                      food where customers can find their favorite food. Have 2
                      roles in one application Responsible for doing two roles
                      in one application, for Customer and Merchant.
                    </p>
                  </div>
                </div>
              </div>
              <div class="col-md-4 mb-3">
                <div
                  class="card"
                  data-aos="zoom-in-up"
                  data-aos-duration="2000"
                >
                  <img
                    src={require("../Component/Images/GGLWeb.png")}
                    class="card-img-top"
                    alt="Projects2"
                  />
                  <div class="card-body">
                    <p class="card-text">
                      Create some new features (GGL Guide Screen Mobile & GGL
                      Guide Detail Screens Mobile) look by implementing a few
                      functions in the GGL App.
                    </p>
                  </div>
                </div>
              </div>
              <div class="col-md-4 mb-3">
                <div
                  class="card"
                  data-aos="flip-left"
                  data-aos-duration="2000"
                  data-aos-delay="200"
                >
                  <img
                    src={require("../Component/Images/SeeEvent.png")}
                    class="card-img-top"
                    alt="Projects3"
                  />
                  <div class="card-body">
                    <p class="card-text">
                      Creating the user interface for the mobile aplication.
                      Implements search future for users search spesification
                      data from the API. Implements CRUD (API Fetching).
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
            <path
              fill="#ffffff"
              fill-opacity="1"
              d="M0,288L60,288C120,288,240,288,360,272C480,256,600,224,720,218.7C840,213,960,235,1080,229.3C1200,224,1320,192,1380,176L1440,160L1440,320L1380,320C1320,320,1200,320,1080,320C960,320,840,320,720,320C600,320,480,320,360,320C240,320,120,320,60,320L0,320Z"
            ></path>
          </svg>
        </div>
      </div>
    </>
  );
};

export default Main;
